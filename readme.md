# Slip

This repository contains all versions of Slip available from the 
[Programming Language Engineering website](http://soft.vub.ac.be/PLE)

This makes it possible to view the differences from previous versions, 
and to make your own branches for exercise sessions and other means.
